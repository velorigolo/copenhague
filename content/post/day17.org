#+TITLE: Jour 17
#+DATE: 2011-08-18T10:17:43Z
#+DRAFT: false
#+AUTHOR: Quentin
#+COVER: day17.jpg
#+PLACE: Nykobing
#+SUMMARY: 
#+MAP: true

** Phrase du jour

'Je m'embourbe merde !' - Mathieu

** Etape 

Stavenhagen - Nykobing

** Kilomètres

117

** Résumé

Début très chiant comme jour d'avant. Perdus dans Rostock. Heureux de quitter
Allemagne ! Rencontré 2 hollandais sur le chemin du ferry qui nous ont bien
aidés. Fin de l'étape au Danemark, superbe lumière et paysages. Enflés à
l'auberge de jeunesse, prix exorbitants. Vengeance dans la cuisine, repas
gratuit !

{{< figure src="/copenhague/media/photos/day17/page15.jpg" class="image-article" width="50%">}}
